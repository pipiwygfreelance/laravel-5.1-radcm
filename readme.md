## Installation

Install Laravel 5.1 using composer:

    composer global require "laravel/installer=~1.1"
    
See: http://laravel.com/docs/master/installation#configuration for more information on installing Laravel 5.1

### Configure Laravel Environment
    
Create a common source folder for laravel projects

    mkdir /var/www/laravel
    cd /var/www/laravel
    
Create a new Laravel application by running the following command:

    laravel new laravel-5.1-radcm
    
Create a symbolic link for the public html folder

    ln -s /var/www/laravel/laravel-5.1-radcm/public /var/www/local.laravel-5.1-radcm.pip.co.za
    
Create a new apache virtual host config file, for example:

    $ sudo nano /etc/apache2/sites-available/local.laravel-5.1-radcm.pip.co.za.conf
    
Paste the contents below into the file, and update according to your configuration:

(Note: You can customize the hostname and IP address based on your requirements)

    <VirtualHost 127.0.1.4:80>
        ServerName local.laravel-5.1-radcm.pip.co.za
        ServerAdmin webmaster@localhost
        DocumentRoot /var/www/local.laravel-5.1-radcm.pip.co.za
        
        ErrorLog ${APACHE_LOG_DIR}/local.laravel-5.1-radcm.pip.co.za.error.log
        CustomLog ${APACHE_LOG_DIR}/local.laravel-5.1-radcm.pip.co.za.access.log combined
        
        <Directory /var/www/local.laravel-5.1-radcm.pip.co.za>
            AllowOverride All
            allow from all
            Options Indexes FollowSymLinks
        </Directory>
    </VirtualHost>
    
Edit Hosts

    sudo nano /etc/hosts
    
Add the following line to hosts

    127.0.1.4   local.laravel-5.1-radcm.pip.co.za

Activate the new virtual host and restart Apache

    sudo a2ensite local.laravel-5.1-radcm.pip.co.za.conf
    sudo service apache2 restart

### Database

Connect to your local mysql server:

    mysql -uroot -p
    
Once authenticated, run the following query on the MySQL command line:

    CREATE SCHEMA `laravel-5.1-radcmdb`; quit;

### Download Project Package Archive

Execute the following commands to download the package archive

    cd ~/Downloads
    wget https://bitbucket.org/pipiwygfreelance/laravel-5.1-radcm/downloads/laravel-5.1-radcm-1.4.2.tar.gz
    
Extract the downloaded package archive, to overwrite default laravel files:

    cd /var/www/laravel/laravel-5.1-radcm
    cp ~/Downloads/laravel-5.1-radcm-1.4.2.tar.gz .
    tar -zxvf laravel-5.1-radcm-1.4.2.tar.gz
    rm laravel-5.1-radcm-1.4.2.tar.gz

Set Apache Permissions

    sudo chown -R www-data:www-data /var/www/laravel/laravel-5.1-radcm
    sudo chown -R www-data:www-data /var/www/local.laravel-5.1-radcm.pip.co.za
    
Update composer to install Laravel Project Dependencies

    composer update
    
Update config/app.php. Find the AppKey in the .env file in the root folder of the project:

    'url' => 'http://local.laravel-5.1-radcm.pip.co.za',
    'key' => env('APP_KEY', '**YourAppKey**'),
    
Uncomment the following ServiceProvider clases in config/app.php

    Bestmomo\Scafold\ScafoldServiceProvider::class,
    Collective\Html\HtmlServiceProvider::class,
    
Uncomment the following Aliases in config/app.php

    'Form'      => Collective\Html\FormFacade::class,
    'Html'      => Collective\Html\HtmlFacade::class,

Publish Vendor Changes to retrieve required auth modules

    php artisan vendor:publish
    
Update .env file and set your database details:

    DB_HOST=localhost
    DB_DATABASE=laravel-5.1-radcmdb
    DB_USERNAME=yourdbuser
    DB_PASSWORD=yourdmpass

Update .env file and set your smtp server details:
    
    MAIL_DRIVER=smtp
    MAIL_HOST=your.smtp.server
    MAIL_PORT=25
    MAIL_USERNAME=your.smtp@user.co.za
    MAIL_PASSWORD=your.smtp.password
    MAIL_ENCRYPTION=null
    
Run migrations to create Application Database Tables:
    
    php artisan migrate
    
Run Seeds to populate Roles table and add the initial Default Admin User
    
    php artisan db:seed
    
Open the url in your browser:
    
    http://local.laravel-5.1-radcm.pip.co.za
    
Use the following details to log in

    User: root@localhost
    Password: SJfgHY86^$
    
