@extends('layouts.sub')

@section('page-title','Project Brief')

@section('content')
    @parent
    <div id="content">
        <div class="row">
            <p><b>This project brief</b> has been devised to assess a developer’s knowledge and skill in PHP/MySQL and Laravel development. This assessment is normally requested of a potential candidate during the interview stage of recruitment.</p>
            <h2>Objective</h2>
            <p>Create a simple client and document management PHP/MySQL application. The system is to be written as a Laravel5+ (preferable) or other PHP MVC application and utilize a MySQL database. Simple documentation of the system is required. There should be a readme.md detailing any specific installation/config requirements.</p>
            <p>Although the following are not requirements, demonstration of these technologies/practices will certainly count in your favour:</p>
            <ul>
                <li>The use of composer packages to make your life easier.</li>
                <li>Good database table definitions and the use of constraints and indexes.</li>
                <li>Well-formatted code with clear comments describing complex programming flow.</li>
                <li>Commenting of each method within classes.</li>
                <li>Object oriented design/approach and use of PHP5’s enhanced OO functionality.</li>
                <li>Basic permissions/config check initially to keep the system portable.</li>
                <li>Graceful Error handling.</li>
            </ul>
            <h3>What you'll need</h3>
            <p>PHP 5.4+ (Laravel 5) 5.5.9+ (Laravel 5.1), MySQL 5.1+ and Apache2+ (LAMP, WAMP or MAMP). You may wish to use another MVC (CodeIgniter, Yii, CakePHP, etc) if you feel Laravel territory is going to take too long to get into.</p>
            <h3>Time required</h3>
            <p>This assessment should be able to be completed within 2-5 days, depending on your MVC knowledge, although additional time can be allowed should you wish to demonstrate some advanced programming techniques.</p>
            <h3>What you will supply us with</h3>
            <p>The finished source code (tar, zip, rar) for review once completed (migrations and seeds should be used for the DB). Should you not use migrations, please include the DB sql dump.</p>
            <p>You may also supply your finished source code to us via Github if you prefer.</p>
        </div>
    </div>
    <footer></footer>
@endsection