<!-- resources/views/emails/password.blade.php -->

Hi {{ $first_name }}!<br/>
<br/>
Your password is: <b>{{ $pass }}</b><br/>
<br/>
To log in to your account now, go to <a href="{{ url('auth/login') }}">{{ url('auth/login') }}</a>