@extends('layouts.restricted')

@section('page-title','Delete User Document')

@section('content')
	@parent
    <div id="content">
        <div class="row">
            <form class="form-control" role="form" method="POST" action="{{ url('/admin/userdocs/delete') }}">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<input type="hidden" name="document_id" value="{{ $document->id }}"/>
                <p>You are about to delete the following document.</p>
				<p style="margin-left: 20px;">
					<b>Title: </b> {{ $document->title }}<br/>
					<b>Filename: </b> {{ $document->filename }}<br/>
					<b>Created: </b> {{ $document->created_at }}<br/>
					<b>Updated: </b> {{ $document->updated_at }}<br/>
					<br/>
					<b>Owner: </b> <a href="/admin/userview/{{ $user->id }}">{{ $user->first_name }} {{ $user->last_name }}</a><br/>
				</p>
				<p>Are you sure you want to continue?</p>
                <p><b>Please note:</b> This action cannot be undone.</p>
				<br/>
				<div class="form-group">
					<div class="col-md-6 col-md-offset-4">
						<button type="submit" class="btn btn-primary button_link">
							Delete
						</button>
                        <a href="{{ $_SERVER["HTTP_REFERER"] }}" class="button_link">Cancel</a>
					</div>
				</div>
			</form>
        </div>
    </div>
    <footer></footer>
@endsection
