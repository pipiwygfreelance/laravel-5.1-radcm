@extends('layouts.restricted')

@section('page-title','Edit User Password')

@section('content')
	@parent
    <div id="content">
        <div class="row">
            @if (session('status'))
				<div class="alert alert-success">
					{{ session('status') }}
				</div>
			@endif
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <div class="alert-message"><strong>Whoops!</strong> There were some problems with your input.</div>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form class="form-control" role="form" method="POST" action="{{ url('/admin/useredit/password') }}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="user_id" value="{{ $user->id }}" />
                <div class="form-multi-col">
					<div class="form-left-col">
                        <div class="form-group">
                            <label class="col-md-4 control-label">Password</label>
                            <div class="col-md-6">
                                <input type="password" class="form-control" name="password">
                            </div>
                        </div>
        
                        <div class="form-group">
                            <label class="col-md-4 control-label">Confirm Password</label>
                            <div class="col-md-6">
                                <input type="password" class="form-control" name="password_confirmation">
                            </div>
                        </div>
                        
                        <div class="col-md-6 col-md-offset-4">
                            <button type="submit" class="btn btn-primary button_link">
                                Save Password
                            </button>
                            <a href="{{ $_SERVER["HTTP_REFERER"] }}" class="button_link">Cancel</a>
                        </div>
                    </div>
                    
					<div class="form-right-col">
                        &nbsp;
                    </div>
                </div>
            </form>
        </div>
    </div>
    <footer></footer>
@endsection
