@extends('layouts.restricted')

@section('page-title','User Documents')

@section('content')
    @parent
    <div id="data_content">
        <div class="row">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="data_listing">
                <tr>
                    <th style="width: auto;">Title</th>
                    <th style="width: auto;">Description</th>
                    <th style="width: auto;">Filename</th>
                    <th style="width: auto;">Mime Type</th>
                    <th style="width: 120px;">Created</th>
                    <th style="width: 120px;">Modified</th>
                    <th style="width: auto;">Actions</th>
                </tr>
                @foreach($documents as $document)
                    <tr><td>
                        {{$document->title}}
                    </td><td>
                        {{$document->description}}
                    </td><td>
                        <a href="{{route('admingetuserdocument', [$document->id,$document->filename])}}">{{$document->filename}}</a>
                    </td><td>
                        {{$document->mime_type}}
                    </td><td>
                        {{$document->created_at}}
                    </td><td>
                        {{$document->updated_at}}
                    </td><td>
                        <a href="/admin/userdocs/delete/{{ $document->id }}">Delete</a>
                    </td></tr>
                @endforeach
            </table>
        </div>
    </div>
    <footer></footer>
@endsection