@extends('layouts.restricted')

@section('page-title','List current Users')

@section('content')
    @parent
    <div id="data_content">
        <div class="row">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="data_listing">
                <tr>
                    <th style="width: auto">Name</th>
                    <th style="width: auto">Email</th>
                    <th style="width: 100px;">Role</th>
                    <th style="width: 200px;">Actions</th>
                </tr>
                @foreach($users as $user)
                    <tr><td>
                        {{$user->first_name}} {{$user->last_name}}
                        @if ($currentuser->id == $user->id)
                             <b>(This is you)</b>
                        @endif
                    </td><td>
                        {{$user->email}}
                    </td><td>
                        {{ $user->role->role_name }}
                    </td><td>
                        <a href="/admin/userdocs/{{ $user->id }}" class="action_link">Documents</a>
                        <a href="/admin/userview/{{ $user->id }}" class="action_link">View</a>
                        <a href="/admin/useredit/{{ $user->id }}" class="action_link">Edit</a>
                        @if ($currentuser->id != $user->id)
                            <a href="/admin/userdel/{{ $user->id }}" class="action_link">Delete</a>
                        @endif
                    </td></tr>
                @endforeach
            </table>
            <br/>
            <a href="/admin/useradd" class="button_link">Add new User</a>
        </div>
    </div>
    <footer></footer>
@endsection