@extends('layouts.restricted')

@section('page-title','Delete User')

@section('content')
	@parent
    <div id="content">
        <div class="row">
            <form class="form-control" role="form" method="POST" action="{{ url('/admin/userdel') }}">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<input type="hidden" name="user_id" value="{{ $user->id }}"/>
                <p>You are about to delete the following user.</p>
				<p style="margin-left: 20px;">
					<b>Name: </b> {{ $user->first_name }} {{ $user->last_name }}<br/>
					<b>Email: </b> {{ $user->email }}<br/>
					<b>Created: </b> {{ $user->created_at }}<br/>
					<b>Updated: </b> {{ $user->updated_at }}<br/>
					@if (count($documents) > 0)
						<br/>
						<a href="/admin/userdocs/{{ $user->id }}">{{ count($documents) }}
						@if (count($documents) == 1)
							document</a> is
						@else
							documents</a> are
						@endif
						currently linked to this user and will be deleted if you continue with this action.
					@endif
				</p>
				<p>Are you sure you want to continue?</p>
                <p><b>Please note:</b> This action cannot be undone.</p>
				<br/>
				<div class="form-group">
					<div class="col-md-6 col-md-offset-4">
						<button type="submit" class="btn btn-primary button_link">
							Delete
						</button>
                        <a href="{{ $_SERVER["HTTP_REFERER"] }}" class="button_link">Cancel</a>
					</div>
				</div>
			</form>
        </div>
    </div>
    <footer></footer>
@endsection
