@extends('layouts.restricted')

@section('page-title','View User')

@section('content')
    @parent
    <div id="content">
        <div class="row">
            <div style="padding: 20px;">
                <div class="form-multi-col">
					<div class="form-left-col">
                        <div class="data-row">
                            <div class="data-column">
                                <label>Title</label>
                            </div><div class="data-column">
                                <div>{{ $user->title }}</div>
                            </div>
                        </div>
                        <div class="data-row">
                            <div class="data-column">
                                <label>First Name</label>
                            </div><div class="data-column">
                                <div>{{ $user->first_name }}</div>
                            </div>
                        </div>
                        <div class="data-row">
                            <div class="data-column">
                                <label>Last Name</label>
                            </div><div class="data-column">
                                <div>{{ $user->last_name }}</div>
                            </div>
                        </div>
                        <br/>
                        <div class="data-row">
                            <div class="data-column">
                                <label>Email</label>
                            </div><div class="data-column">
                                <div>{{ $user->email }}</div>
                            </div>
                        </div>
                        <br/>
                        <div class="data-row">
                            <div class="data-column">
                                <label>Role</label>
                            </div><div class="data-column">
                                <div>{{ $user->role->role_name }}</div>
                            </div>
                        </div>
                        <br/>
                        <div class="data-row">
                            <div class="data-column">
                                <label>Telephone</label>
                            </div><div class="data-column">
                                <div>{{ $user->tel_number }}</div>
                            </div>
                        </div>
                        <div class="data-row">
                            <div class="data-column">
                                <label>Fax</label>
                            </div><div class="data-column">
                                <div>{{ $user->fax_number }}</div>
                            </div>
                        </div>
                        <br/>
                        <div class="data-row">
                            <div class="data-column">
                                <label>Date Created</label>
                            </div><div class="data-column">
                                <div>{{ $user->created_at }}</div>
                            </div>
                        </div>
                        <div class="data-row">
                            <div class="data-column">
                                <label>Date Updated</label>
                            </div><div class="data-column">
                                <div>{{ $user->updated_at }}</div>
                            </div>
                        </div>
                    </div>
					<div class="form-right-col">
                        <div class="data-row">
                            <div class="data-column">
                                <label>Physical Address</label>
                            </div><div class="data-column">
                                <div>{{ $user->phys_address_l1 }}</div>
                            </div>
                        </div>
                        <div class="data-row">
                            <div class="data-column">
                                &nbsp;
                            </div><div class="data-column">
                                <div>{{ $user->phys_address_l2 }}</div>
                            </div>
                        </div>
                        <div class="data-row">
                            <div class="data-column">
                                &nbsp;
                            </div><div class="data-column">
                                <div>{{ $user->phys_address_l3 }}</div>
                            </div>
                        </div>
                        <div class="data-row">
                            <div class="data-column">
                                &nbsp;
                            </div><div class="data-column">
                                <div>{{ $user->phys_address_l4 }}</div>
                            </div>
                        </div>
                        <div class="data-row">
                            <div class="data-column">
                                <label>Area Code</label>
                            </div><div class="data-column">
                                <div>{{ $user->phys_area_code }}</div>
                            </div>
                        </div>
                        <br/>
                        <div class="data-row">
                            <div class="data-column">
                                <label>Postal Address</label>
                            </div><div class="data-column">
                                <div>{{ $user->post_address_l1 }}</div>
                            </div>
                        </div>
                        <div class="data-row">
                            <div class="data-column">
                                &nbsp;
                            </div><div class="data-column">
                                <div>{{ $user->post_address_l2 }}</div>
                            </div>
                        </div>
                        <div class="data-row">
                            <div class="data-column">
                                &nbsp;
                            </div><div class="data-column">
                                <div>{{ $user->post_address_l3 }}</div>
                            </div>
                        </div>
                        <div class="data-row">
                            <div class="data-column">
                                &nbsp;
                            </div><div class="data-column">
                                <div>{{ $user->post_address_l4 }}</div>
                            </div>
                        </div>
                        <div class="data-row">
                            <div class="data-column">
                                <label>Postal Code</label>
                            </div><div class="data-column">
                                <div>{{ $user->post_code }}</div>
                            </div>
                        </div>
                    </div>
                </div>
                <br/><br/>
                <div>
                    <a href="/admin/useredit/{{ $user->id }}" class="button_link">Edit User</a>
                    <a href="{{ $_SERVER["HTTP_REFERER"] }}" class="button_link">Back</a>
                </div>
            </div>
        </div>
    </div>
    <footer></footer>
@endsection