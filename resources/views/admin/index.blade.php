@extends('layouts.restricted')

@section('page-title','Administration')

@section('content')
    @parent
    <div id="data_content">
        <div class="row">
            <div style="padding: 20px;">
                <b>Welcome {{ $user->first_name }}</b>,
                <p>Since you are an Administrator of this site, you have a few power functions that you can use to manage and maintain system information and variables.</p>
                <p>Please use the links on the left to access functionality available within the system. Alternatively, use the quick links below to access key areas quickly.</p>
                <div style="margin-left: 20px;">
                    <b>Users</b>
                    <ul>
                        <li><a href="/admin/userlist">List all current Users</a></li>
                        <li><a href="/admin/useradd">Add a new User</a></li>
                    </ul>
                </div>
                <p><b>Please note: </b>To manage, view or delete user documentation, see the <a href="/admin/userlist">User List</a> page.</p>
            </div>
        </div>
    </div>
    <footer></footer>
@endsection