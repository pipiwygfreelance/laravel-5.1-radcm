@extends('layouts.sub')

@section('page-title','External Tools &amp; Resources')

@section('content')
    @parent
    <div id="content">
        <div class="row">
            <p><b>This project is shipped</b> with the following tools and resources, which was created in conjunction with the application in order to manage and maintain project requirements.</p>
            <ul>
                <li><a href="https://bitbucket.org/pipiwyg/radcmv2/overview" target="_blank">Project GIT Repository</a> on <a href="https://bitbucket.org/" target="_blank">BitBucket</a></li>
                <li><a href="https://trello.com/b/EPDcX9i8/rhinoafrica-document-and-client-management" target="_blank">Project Task Board</a> on <a href="https://trello.com/" target="_blank">Trello</a></li>
                <li><a href="https://wakatime.com/project/rhdapp?start=2015-07-19&end=2015-07-25" target="_blank">Project Time Tracking</a> on <a href="https://wakatime.com/" target="_blank">WakaTime</a></li>
                <li><a href="http://laravel.com/docs/5.1/" target="_blank">Laravel 5.1 Documentation</a></li>
                <li><a href="http://www.pip.co.za/" target="_blank">Author's Web Site</a></li>
                <li><a href="http://www.rhinoafrica.com/" target="_blank">Rhino Africa</a></li>
                <li><a href="https://docs.google.com/document/d/1wI3ZCyCKERDKwuqHvtquql3UOdoaK8pLDNcXMKopeXw/edit?usp=sharing" target="_blank">Curriculum Vitae of Quintin L. Stoltz</a> on <a href="https://docs.google.com/" target="_blank">Google Docs</a></li>
            </ul>
        </div>
    </div>
    <footer></footer>
@endsection