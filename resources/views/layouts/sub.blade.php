@extends('layouts.master')

@section('appName', 'RA: DCM')

@section('title')
    @yield('page-title') - @parent
@endsection