@extends('layouts.basic')

@section('meta-tags')
@parent
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
@endsection

@section('html-document-favicons')
    <link href="{{ asset('favicon.ico') }}" type="image/x-icon" rel="icon"/>
    <link href="{{ asset('favicon.ico') }}" type="image/x-icon" rel="shortcut icon"/>
@endsection

@section('html-document-stylesheets')
    <link rel="stylesheet" href="{{ asset('css/enhanced.css') }}"/>
@endsection

@section('html-body')
    @parent
    <div>Enhanced Layout Template</div>
@endsection

@section('html-document-title')
    <title>Enhanced Layout Template</title>
@endsection

@section('html-head')
    @parent
    @yield('html-document-favicons')
    
    @yield('html-document-stylesheets')
@endsection