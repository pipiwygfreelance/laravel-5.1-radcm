@extends('layouts.enhanced')

@section('page-title', 'Welcome')
@section('appSubTitle','Rhino Africa')
@section('appTitle', 'Document &amp; Client Management')
@section('title', 'Practical Assessment Laravel Application')
@section('appName', 'Rhino Africa: Document &amp; Client Management')

@section('html-document-stylesheets')
    @parent
    <link rel="stylesheet" href="{{ asset('css/fonts/fonts.css') }}"/>
    <link rel="stylesheet" href="{{ asset('css/master.css') }}"/>
    <link rel="stylesheet" href="{{ asset('css/main.css') }}"/>
    <link rel="stylesheet" href="{{ asset('css/data.css') }}"/>
@endsection

@section('html-document-title')
    <title>@yield('title') - @yield('appName')</title>
@endsection

@section('page-content-header')
    <header>
        <h3>@yield('page-title')</h3>
    </header>
@endsection

@section('memberLogin')
    <div class="member-login">
        <form method="POST" action="/auth/login">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div>
                <input type="email" name="email" autocomplete="off" value="{{ old('email') }}" placeholder="Email / Username">
            </div>
            <div>
                <input type="password" name="password" autocomplete="off" id="password" placeholder="Password">
            </div>
            <div>
                <button type="submit" class="btn submit">Login</button>
            </div>
        </form>
    </div>
@endsection

@section('profileModule')
    <div class="profile-block">
        @if (Auth::check())
            Hello {{ Auth::user()->first_name }} ({{ Auth::user()->email }})
            <ul>
                <li><a href="/users">Dashboard</a></li>
                <li><a href="/users/profile">Profile</a></li>
                 @if (Auth::user()->role_id == "1")
                    <li><a href="/admin/">Administration</a></li>
                @endif
                <li><a href="/auth/logout">Logout</a></li>
            </ul>
        @endif
    </div>
@endsection

@section('memberProfile')
    <div class="member-profile-module">
        @if (Auth::check())
            @yield('profileModule')
        @else
            @yield('memberLogin')
        @endif
    </div>
@endsection

@section('document-page-header')
    <header class="document_header">
        <div class="header-logo"></div>
        <div class="header-title">
            <h5>@yield('appSubTitle')</h5>
            <h2>@yield('appTitle')</h2>
        </div>
    </header>
    <header class="document_subheader">
        @yield('memberProfile')
    </header>
@endsection

@section('navigation')
    <ul>
        <!--<li><a href="/" class="{{ (isset($name) && $name == "welcome") ? 'selected' : '' }}">Welcome</a></li>-->
        <li><a href="/intro" class="{{ (isset($name) && $name == "intro") ? 'selected' : '' }}">Project Brief</a></li>
        <li><a href="/sysrequirements" class="{{ (isset($name) && $name == "sysrequirements") ? 'selected' : '' }}">System Requirements</a></li>
    </ul>
    <ul>
        <li><a href="/general" class="{{ (isset($name) && $name == "general") ? 'selected' : '' }}">General</a></li>
        <li><a href="/resources" class="{{ (isset($name) && $name == "resources") ? 'selected' : '' }}">Tools &amp; Resources</a></li>
    </ul>
    <ul class="width-spacer">
        @if (!Auth::check())
            <li><a href="/auth/login" class="{{ (isset($name) && $name == "login") ? 'selected' : '' }}">Login</a></li>
        @else
            <li><a href="/users" class="{{ (isset($name) && $name == "user_dashboard") ? 'selected' : '' }}">Dashboard</a></li>
            <li><a href="/users/documents" class="{{ (isset($name) && $name == "user_documents") ? 'selected' : '' }}">Documents</a></li>
            <li><a href="/users/profile" class="{{ (isset($name) && $name == "user_profile") ? 'selected' : '' }}">Profile</a></li>
        @endif
    </ul>
    @if (Auth::check())
        @if (Auth::user()->role_id == "1")
        <ul class="width-spacer">
            <li><a href="/admin" class="{{ (isset($name) && $name == "admin_dashboard") ? 'selected' : '' }}">Admininistration</a></li>
            <li><a href="/admin/userlist" class="{{ (isset($name) && $name == "admin_userlist") ? 'selected' : '' }}">List Users</a></li>
        </ul>
        @endif
    @endif
    @if (Auth::check())
        <ul class="width-spacer">
            <li><a href="/auth/logout" class="{{ (isset($name) && $name == "logout") ? 'selected' : '' }}">Logout</a></li>
        </ul>
    @endif
@endsection

@section('sidenav')
    <nav class="sidenav">
        @yield('navigation')
    </nav>
@endsection

@section('content')
    @yield('page-content-header')
@endsection

@section('document-page-container')
    <div id="container">
        @yield('sidenav')
        <div class="content">
            @yield('content')
        </div>
    </div>
@endsection

@section('document-page-footer')
    <div class="document-footer">
        <footer class="document_footer">
            <div class="app_logo">
                <img src="{{ asset('images/rhinoa.png') }}" />
            </div>
            <div class="footernav">
                <nav class="footernavbar">
                    @yield('navigation')
                </nav>
            </div>
        </footer>
    </div>
@endsection

@section('page-content')
    @yield('document-page-header')
    
    @yield('document-page-container')
    
    @yield('document-page-footer')
@endsection

@section('html-body')
    <div class="page wrapper">
        @yield('page-content')
    </div>
@endsection