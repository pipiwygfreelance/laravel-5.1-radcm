@extends('layouts.master')

@section('page-title', 'Welcome')
@section('appSubTitle','Rhino Africa')
@section('appTitle', 'Document &amp; Client Management')
@section('title', 'Practical Assessment Laravel Application')
@section('appName', 'Rhino Africa: Document &amp; Client Management')

@section('memberLogin')
    
@endsection

@section('page-content-header')
    <header>
        <h3>@yield('page-title')</h3>
    </header>
@endsection

@section('content')
    @yield('page-content-header')
    @yield('memberLogin')
@endsection