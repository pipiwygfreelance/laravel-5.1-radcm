@section('html-body')
    <div>Basic Layout Template</div>
@endsection

@section('meta-tags')
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
@endsection

@section('html-document-title')
    <title>Basic Layout Template</title>
@endsection

@section('html-head')
    @yield('meta-tags')
    @yield('html-document-title')
@endsection

@section('document')
<!DOCTYPE html>
<html>
    <head>
    @yield('html-head')
    </head>
    <body>
    @yield('html-body')
    </body>
</html>
@show