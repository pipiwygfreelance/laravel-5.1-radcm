@extends('layouts.client')

@section('appName', 'RA: DCM')

@section('title')
    @yield('page-title') - @parent
@endsection