@extends('layouts.master')

@section('page-title','Welcome')

@section('content')
    @parent
    <div id="content">
        <div class="row">
            <p><b>Welcome to this simple</b> Document and Client Management Application written in Laravel 5.1, a PHP Framework that is clearly yet to prove that it's simpler than CakePHP.</p>
            <p>Although Apache and Mod Rewrite proved to be UTTERLY annoying this evening, costing me HOURS of trial and error, now that I finally have some views and controllers in place, I am a little annoyed with how the Laravel documentation claims that it's so simple, while I would've had a CakePHP Application going in no time. Granted, I know Cake... But Cake's learning curve took me a quater of the time.</p>
            <p>But Alas. If anything comes from this assessment, at least it would've provided a learning curve and challenge, and I'll walk away being able to say that I too have Laravel experience, and with a little bit of effort, I'm sure I can master it.</p>
            <p>Now, let's see if we can get some proper database integration and interaction going. <b>Here begineth my app</b>. May it be worth our while.</p>
        </div>
    </div>
    <footer></footer>
@endsection