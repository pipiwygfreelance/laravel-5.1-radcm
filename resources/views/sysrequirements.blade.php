@extends('layouts.sub')

@section('page-title','System Requirements')

@section('content')
    @parent
    <div id="content">
        <div class="row">
            <h2>General</h2>
            <p><b>Each Client needs</b> to have a secure login based on their email address and an encrypted password. There should be 2 roles, Administrator and Client. Once authenticated Clients can perform the following actions:</p>
            <ul>
                <li>Log in (passwords auto-generated and mailed to client)</li>
                <li>View and Edit their details.</li>
                <li>Upload multiple documents and Delete uploaded documents.</li>
                <li>Reset their password</li>
            </ul>
            
            <h3>Administration</h3>
            <p>Administrators should be able to:</p>
            <ul>
                <li>Add or Register new Clients</li>
                <li>View and Edit all Clients</li>
                <li>View and manage all Client documents.</li>
            </ul>
            
            <h3>Registration</h3>
            <p>Clients should not be allowed to register and should only be created by Administrators. The client document listing should show the last modified documents in descending order for each client.</p>
            <p>Note * char indicates the field is mandatory.</p>
            <p>The following information needs to be stored for each Administrator</p>
            <ul>
                <li>First name *</li>
                <li>Last name *</li>
                <li>Email</li>
                <li>Password</li>
            </ul>
            
            <p>The following information needs to be stored for each Client</p>
            <ul>
                <li>First name *</li>
                <li>Last name *</li>
                <li>Title</li>
                <li>Email Address *</li>
                <li>Password</li>
                <li>Telephone Number</li>
                <li>Fax Number</li>
                <li>Physical Address</li>
                <li>Postal Address</li>
            </ul>
            
            <h3>Document Management</h3>
            <p>The Following Information needs to be stored for each document</p>
            <ul>
                <li>Title</li>
                <li>Filename</li>
                <li>Description</li>
                <li>Mime Type</li>
                <li>Date Created</li>
            </ul>
        </div>
    </div>
    <footer></footer>
@endsection