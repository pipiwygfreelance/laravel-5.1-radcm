@extends('layouts.client')

@section('page-title','Add new Document')

@section('content')
	@parent
    <div id="content">
        <div class="row">
            @if (count($errors) > 0)
				<div class="alert alert-danger">
					<div class="alert-message"><strong>Whoops!</strong> There were some problems with your input.</div>
					<ul>
						@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
			@endif
            
            <form class="form-control" role="form" method="POST" action="{{ url('/users/documentadd') }}" accept-charset="UTF-8" novalidate="novalidate" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-multi-col">
					<div class="form-left-col">
						<div class="form-group">
							<label class="col-md-4 control-label">Title</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="title" value="{{ old('title') }}">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Description</label>
							<div class="col-md-6">
                                <textarea class="form-control" name="description" style="width: 450px; height: 140px;">{{ old('description') }}</textarea>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Document</label>
							<div class="col-md-6">
                                <input class="form-control" name="document" type="file">
							</div>
						</div>
                    </div>
				</div>
				<br/>
				<div class="form-group">
					<div class="col-md-6 col-md-offset-4">
						<button type="submit" class="btn btn-primary button_link">
							Add Document
						</button>
						<a href="{{ $_SERVER["HTTP_REFERER"] }}" class="button_link">Cancel</a>
					</div>
				</div>
			</form>
        </div>
    </div>
    <footer></footer>
@endsection