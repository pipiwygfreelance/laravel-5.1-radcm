@extends('layouts.client')

@section('page-title','Edit Profile')

@section('content')
	@parent
    <div id="content">
        <div class="row">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <div class="alert-message"><strong>Whoops!</strong> There were some problems with your input.</div>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form class="form-control" role="form" method="POST" action="{{ url('/users/edit') }}">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<input type="hidden" name="role_id" value="{{ $user->role->id }}"/>
				
				<div class="form-multi-col">
					<div class="form-left-col">
						<div class="form-group">
							<label class="col-md-4 control-label">Title</label>
							<div class="col-md-6">
								<input type="text" class="form-control short" name="title" value="{{ $user->title }}">
							</div>
						</div>
		
						<div class="form-group">
							<label class="col-md-4 control-label">First Name</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="first_name" value="{{ $user->first_name }}">
							</div>
						</div>
		
						<div class="form-group">
							<label class="col-md-4 control-label">Last Name</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="last_name" value="{{ $user->last_name }}">
							</div>
						</div>
		
						<div class="form-group">
							<label class="col-md-4 control-label">E-Mail Address</label>
							<div class="col-md-6">
								<input type="email" class="form-control" name="email" value="{{ $user->email }}">
							</div>
						</div>
							
						<div class="form-group">
							<label class="col-md-4 control-label">Telephone</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="tel_number" value="{{ $user->tel_number }}">
							</div>
						</div>
		
						<div class="form-group">
							<label class="col-md-4 control-label">Fax</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="fax_number" value="{{ $user->fax_number }}">
							</div>
						</div>
					</div>
					<div class="form-right-col">
						<div class="form-group">
							<label class="col-md-4 control-label">Physical Address</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="phys_address_l1" value="{{ $user->phys_address_l1 }}">
							</div>
							<div class="col-md-6">
								<input type="text" class="form-control" name="phys_address_l2" value="{{ $user->phys_address_l2 }}">
							</div>
							<div class="col-md-6">
								<input type="text" class="form-control" name="phys_address_l3" value="{{ $user->phys_address_l3 }}">
							</div>
							<div class="col-md-6">
								<input type="text" class="form-control" name="phys_address_l4" value="{{ $user->phys_address_l4 }}">
							</div>
							<div class="col-md-6">
								<input type="text" class="form-control" name="phys_area_code" value="{{ $user->phys_area_code }}">
							</div>
						</div>
		
						<div class="form-group">
							<label class="col-md-4 control-label">Postal Address</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="post_address_l1" value="{{ $user->post_address_l1 }}">
							</div>
							<div class="col-md-6">
								<input type="text" class="form-control" name="post_address_l2" value="{{ $user->post_address_l2 }}">
							</div>
							<div class="col-md-6">
								<input type="text" class="form-control" name="post_address_l3" value="{{ $user->post_address_l3 }}">
							</div>
							<div class="col-md-6">
								<input type="text" class="form-control" name="post_address_l4" value="{{ $user->post_address_l4 }}">
							</div>
							<div class="col-md-6">
								<input type="text" class="form-control" name="post_code" value="{{ $user->post_code }}">
							</div>
						</div>

					</div>
				</div>
				<br/>
				<div class="form-group">
					<div class="col-md-6 col-md-offset-4">
						<button type="submit" class="btn btn-primary button_link">
							Save
						</button>
						<a href="{{ $_SERVER["HTTP_REFERER"] }}" class="button_link">Cancel</a>
					</div>
				</div>
			</form>
        </div>
    </div>
    <footer></footer>
@endsection
