@extends('layouts.client')

@section('page-title','<div style="float: right; margin: 0; padding: 0;"><a href="/users/documentadd" class="button_link" style="margin: 0;font-size: 11px;position: relative;top: -5px; right: -15px;">Add new Document</a></div>Documents')

@section('content')
    @parent
    <div id="data_content">
        <div class="row">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="data_listing">
                @foreach($documents as $document)
                <tr>
                    <td style="padding: 0; border-bottom: 0;">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="data_listing" style="line-height: 1.8em;">
                            <tr>
                                <th rowspan="2" class="doc_image">
                                    <img src="/images/doc.png"/>
                                </th>
                                <th><h3><a href="{{route('getdocument', $document->filename)}}">{{$document->title}}</a></h3>{{$document->mime_type}}</th>
                                <th style="font-weight: normal; width: 200px;">
                                    <b>Created:</b> {{$document->created_at}}<br/><b>Modified:</b> {{$document->updated_at}}
                                </th>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <b>Description: </b>{{$document->description}}
                                    <br/><br/>
                                    <b>Filename: </b> {{ $document->filename }}<br/>
                                    <a href="{{route('getdocument', [$document->id,$document->filename])}}">View</a> | <a href="/users/documentdelete/{{ $document->id }}">Delete</a><br/>
                                    <br/>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                @endforeach
            </table>
        </div>
    </div>
    <footer></footer>
@endsection