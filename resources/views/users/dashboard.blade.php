@extends('layouts.client')

@section('page-title','Dashboard')

@section('content')
    @parent
    <div id="data_content">
        <div class="row">
            <div style="padding: 20px;">
                <b>Welcome {{ $user->first_name }}</b>,
                <p>This site allows you to upload and manage documentation.</p>
                <p>Please use the links on the left to access functionality available within the system. Alternatively, use the quick links below to access key areas quickly.</p>
                <div style="margin-left: 20px;">
                    <b>Documents</b>
                    <ul>
                        <li><a href="/users/documents">List all your current documents</a></li>
                        <li><a href="/users/documentadd">Add a new document</a></li>
                    </ul>
                    <b>Profile</b>
                    <ul>
                        <li><a href="/users/profile">View your Profile</a></li>
                        <li><a href="/users/edit">Edit your Profile</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <footer></footer>
@endsection