@extends('layouts.restricted')

@section('page-title','Unauthorized Access')

@section('content')
    @parent
    <div id="data_content">
        <div class="row">
            <div style="padding: 20px;">
                <div class="alert alert-danger">
                    <div class="alert-message"><strong>Whoops!</strong> You do not have sufficient priviledges on your account to access the page requested.</div>
                    <p>If this is in error, please contact the web site adminitrator</p>
                </div>
            </div>
        </div>
    </div>
    <footer></footer>
@endsection