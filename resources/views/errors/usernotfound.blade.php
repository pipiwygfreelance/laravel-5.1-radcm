@extends('layouts.restricted')

@section('page-title','User Not Found')

@section('content')
    @parent
    <div id="data_content">
        <div class="row">
            <div style="padding: 20px;">
                <div class="alert alert-danger">
                    <div class="alert-message"><strong>Whoops!</strong> The requested User with ID: {{ $user_id }} is invalid or could not be found.</div>
                    <p>If you are certain that the user does exist, and the problem persists, please contact the web site adminitrator</p>
                </div>
            </div>
        </div>
    </div>
    <footer></footer>
@endsection