@extends('layouts.sub')

@section('page-title','Register')

@section('content')
	@parent
    <div id="content">
        <div class="row">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <div class="alert-message"><strong>Whoops!</strong> There were some problems with your input.</div>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form class="form-horizontal" role="form" method="POST" action="{{ url('/auth/register') }}">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<input type="hidden" name="role_id" value="1"/>
				
				<div class="form-group">
					<label class="col-md-4 control-label">Title</label>
					<div class="col-md-6">
						<input type="text" class="form-control" name="title" value="{{ old('title') }}">
					</div>
				</div>

				<div class="form-group">
					<label class="col-md-4 control-label">First Name</label>
					<div class="col-md-6">
						<input type="text" class="form-control" name="first_name" value="{{ old('first_name') }}">
					</div>
				</div>

				<div class="form-group">
					<label class="col-md-4 control-label">Last Name</label>
					<div class="col-md-6">
						<input type="text" class="form-control" name="last_name" value="{{ old('last_name') }}">
					</div>
				</div>

				<div class="form-group">
					<label class="col-md-4 control-label">E-Mail Address</label>
					<div class="col-md-6">
						<input type="email" class="form-control" name="email" value="{{ old('email') }}">
					</div>
				</div>

				<div class="form-group">
					<label class="col-md-4 control-label">Password</label>
					<div class="col-md-6">
						<input type="password" class="form-control" name="password">
					</div>
				</div>

				<div class="form-group">
					<label class="col-md-4 control-label">Confirm Password</label>
					<div class="col-md-6">
						<input type="password" class="form-control" name="password_confirmation">
					</div>
				</div>

				<div class="form-group">
					<label class="col-md-4 control-label">Telephone</label>
					<div class="col-md-6">
						<input type="text" class="form-control" name="tel_number" value="{{ old('tel_number') }}">
					</div>
				</div>

				<div class="form-group">
					<label class="col-md-4 control-label">Fax</label>
					<div class="col-md-6">
						<input type="text" class="form-control" name="fax_number" value="{{ old('fax_number') }}">
					</div>
				</div>

				<div class="form-group">
					<label class="col-md-4 control-label">Physical Address</label>
					<div class="col-md-6">
						<input type="text" class="form-control" name="phys_address_l1" value="{{ old('phys_address_l1') }}">
					</div>
					<div class="col-md-6">
						<input type="text" class="form-control" name="phys_address_l2" value="{{ old('phys_address_l2') }}">
					</div>
					<div class="col-md-6">
						<input type="text" class="form-control" name="phys_address_l3" value="{{ old('phys_address_l3') }}">
					</div>
					<div class="col-md-6">
						<input type="text" class="form-control" name="phys_address_l4" value="{{ old('phys_address_l4') }}">
					</div>
					<div class="col-md-6">
						<input type="text" class="form-control" name="phys_area_code" value="{{ old('phys_area_code') }}">
					</div>
				</div>

				<div class="form-group">
					<label class="col-md-4 control-label">Postal Address</label>
					<div class="col-md-6">
						<input type="text" class="form-control" name="post_address_l1" value="{{ old('post_address_l1') }}">
					</div>
					<div class="col-md-6">
						<input type="text" class="form-control" name="post_address_l2" value="{{ old('post_address_l2') }}">
					</div>
					<div class="col-md-6">
						<input type="text" class="form-control" name="post_address_l3" value="{{ old('post_address_l3') }}">
					</div>
					<div class="col-md-6">
						<input type="text" class="form-control" name="post_address_l4" value="{{ old('post_address_l4') }}">
					</div>
					<div class="col-md-6">
						<input type="text" class="form-control" name="post_code" value="{{ old('post_code') }}">
					</div>
				</div>

				<div class="form-group">
					<div class="col-md-6 col-md-offset-4">
						<button type="submit" class="btn btn-primary">
							Register
						</button>
					</div>
				</div>
			</form>
        </div>
    </div>
    <footer></footer>
@endsection
