@extends('layouts.sub')

@section('page-title','Reset Password')

@section('content')
	@parent
	<div id="content">
        <div class="row">
            @if (session('status'))
				<div class="alert alert-success">
					{{ session('status') }}
				</div>
			@endif
			@if (count($errors) > 0)
				<div class="alert alert-danger">
					<div class="alert-message"><strong>Whoops!</strong> There were some problems with your input.</div>
					<ul>
						@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
			@endif
			
			<div class="member-login">
				<form role="form" method="POST" action="{{ url('/password/email') }}">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<div>
						<input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email / Username">
					</div>
					<div>
						<button type="submit" class="btn btn-primary">
							Send Password Reset Link
						</button>
					</div>
				</form>
			</div>
        </div>
    </div>
    <footer></footer>
@endsection
