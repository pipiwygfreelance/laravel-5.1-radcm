@extends('layouts.sub')

@section('page-title','Client Login')

@section('memberLogin')
    @parent
@endsection

@section('memberProfile')
    <div class="member-profile-module"></div>
@endsection

@section('content')
	@parent
	<div id="content">
        <div class="row">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <div class="alert-message"><strong>Whoops!</strong> There were some problems with your input.</div>
					<ul>
						@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
            @endif
			<div class="member-login">
				<form method="POST" action="{{ url('/auth/login') }}">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<div>
						<input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email / Username">
					</div>
					<div>
						<input type="password" class="form-control" name="password" placeholder="Password">
					</div>
					<div class="form-group">
						<div>
							<div class="checkbox">
								<label>
									<input type="checkbox" name="remember"> Remember Me
								</label>
							</div>
						</div>
					</div>
					<div>
						<button type="submit" class="btn btn-primary submit">Login</button>
						
						<a class="btn btn-link" href="{{ url('/password/email') }}">Forgot Your Password?</a>
					</div>
				</form>
			</div>
        </div>
    </div>
    <footer></footer>
@endsection
