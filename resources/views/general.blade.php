@extends('layouts.sub')

@section('page-title','General Requirements')

@section('content')
    @parent
    <div id="content">
        <div class="row">
            <p></p>
            <ul>
                <li>The system should be informative and easy to use.</li>
                <li>The system should perform validation on data.</li>
                <li>The interface should be reasonable and work in all major browsers.</li>
                <li>The system should be developed in such a way to allow for scalability, flexibility, easy maintenance and code re-use/usability.</li>
            </ul>
        </div>
    </div>
    <footer></footer>
@endsection