<?php
// Controller Namespace
namespace App\Http\Controllers;

// Components
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

// Models
use App\User;
use App\Role;
use App\Document;

// Engines
use Storage;
use Validator;
use Mail;
use File;

/**
 * AdminController responsible for all Admin Related Functions
 */
class AdminController extends Controller {
    /**
     * Instantiate a new AdminController instance.
     *
     * @return void
     */
    public function __construct() {
		// Set the middleware key to seperate Client and Admin authenticated access
        $this->middleware('adminauth');
    }
    
    /**
     * Load Dashboard / Index View
     *
     * @return Response
     */
    public function index() {
        return view('admin.index', ['name' => 'admin_dashboard', 'user' => Auth::user()]);
    }
    
    /**
     * Load Admin User Add View
     */
    public function getUserAdd() {
        return view('admin.useradd', ['name' => 'admin_useradd', 'roles' => Role::all()]);
    }
	
    /**
     * Show the admin delete user document view
     */
    public function deleteUserDoc($document_id) {
		// Retrieve document
		$document = Document::find($document_id);
		
		// Only continue if a valid document record was found
		if ($document) {
			// Get the User ID assigned to the document
			$user_id = $document->user_id;
			
			// Get the user associated with the document
			$user = User::find($user_id);
			
			if ($user) // Return the Delete Document confirmation View
				return view('admin.userdocdelete', ['name' => 'admin_userdocdel', 'document' => $document, 'user' => $user]);
			
			// Invalid User View
			return view('errors.usernotfound', ['user_id' => $user_id]);
		}
		
		// Invalid Document View
		return view('errors.documentnotfound', ['document_id' => $document_id]);
    }
	
	/**
     * Handle a document delete request
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
	public function postDeleteUserDoc(Request $request) {
        // Get Data
        $data = $request->all();
        
        // Get User ID
        $document_id = $data["document_id"];
		
        // Find Document
        $document = Document::find($document_id);
		
		// Only continue if a valid document record was found
		if ($document) {
			$user_id = $document->user_id;
			
			// Delete physical file from storage
			File::delete(storage_path('documents').'/users/'.$user_id.'/'.$document->filename);
			
			// Delete User
			$document->delete();
			
			// Redirect
			return redirect("/admin/userdocs/".$user_id);
		}
		
		// Invalid Document View
		return view('errors.documentnotfound', ['document_id' => $document_id]);
	}
	
	/**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postUserAdd(Request $request) {
		// Get a new User Object so that we can use the validator in the model
		$user = new User();
		
        // Run validator
        $validator = $user->addValidator($request->all());
		
		// Handle validation
        if ($validator->fails()) // Validation failed. Throw Exception
            $this->throwValidationException($request, $validator);
		else {
            // Generate a Random String to be used as Auto Generated User Password
            $pass = str_random(12);
            
            // Define email user object
            $emailUser = [
                'first_name'        =>      $request['first_name'],
                'last_name'         =>      $request['last_name'],
                'email'             =>      $request['email'],
                'pass'              =>      $pass
            ];
            
            // Create User
            $this->create($request->all(),$pass);
            
            // Set Email View Variable
            $view = "emails.autopass";
            
            // Send the User Login Details
            $this->sendUserLoginDetails($emailUser,$view);
        }
		
        // Redirect
        return redirect("/admin/userlist");
    }
    
    /**
     * Show the admin add user form.
     */
    public function getUserList() {
        return view('admin.userlist', ['name' => 'admin_userlist', 'users' => User::all(),'currentuser' => Auth::user()]);
    }
    
    /**
     * Show User Delete View
     */
    public function getUserDel($user_id) {
		// Get the User Object
		$user = User::find($user_id);
		
		// Only continue if we have a valid user
		if ($user) {
			// Get all user docs
			$documents = $user->documents()->get();
			
			// Return View
			return view('admin.userdel', ['name' => 'admin_userdel', 'user' => $user, 'documents' => $documents]);
		}
		
		// Invalid User View
		return view('errors.usernotfound', ['user_id' => $user_id]);
    }
	
	/**
     * Handle a user delete request
     *
     * @param \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postUserDel(Request $request) {
        // Get Data
        $data = $request->all();
        
        // Get User ID
        $user_id = $data["user_id"];
        
        // Find User
        $user = User::find($user_id);
		
		// Only continue if we have a valid user
		if ($user) {
			// Get all user docs
			$user_docs = $user->documents()->get();
			
			// Iterate through user docs, and delete each entry
			foreach($user_docs as $user_doc) {
				// Delete physical file from storage
				File::delete(storage_path('documents').'/users/'.$user_doc->user_id.'/'.$user_doc->filename);
				// Delete user
				$user_doc->delete();
			}
			
			// Delete User
			$user->delete();
			
			// Redirect
			return redirect("/admin/userlist");
		}
		
		// Invalid User View
		return view('errors.usernotfound', ['user_id' => $user_id]);
    }
    
    /**
     * Show User View View
     */
    public function getUserView($user_id) {
        return view('admin.userview', ['name' => 'admin_userview', 'user' => User::find($user_id)]);
    }
    
    /**
     * Show User View View
     */
    public function getUserEdit($user_id) {
        return view('admin.useredit', ['name' => 'admin_useredit', 'user' => User::find($user_id), 'roles' => Role::all()]);
    }
	
	/**
     * Handle a user edit request
     *
     * @param \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postUserEdit(Request $request) {
        // Get Posted Data
        $data = $request->all();
        
        // Get User ID
        $user_id = $data["user_id"];
        
        // Get User Object
        $user = User::find($user_id);
        
		// Only continue if we have a valid user
		if ($user) {
			// Validate input
			$validator = $user->editValidator($data);
			
			// Handle Validation Errors
			if ($validator->fails())
				$this->throwValidationException($request, $validator);
			else {
				// Validation suceeded. Update User Data
				$user->title = $data['title'];
				$user->first_name = $data['first_name'];
				$user->last_name = $data['last_name'];
				$user->email = $data['email'];
				$user->tel_number = $data['tel_number'];
				$user->fax_number = $data['fax_number'];
				$user->phys_address_l1 = $data['phys_address_l1'];
				$user->phys_address_l2 = $data['phys_address_l2'];
				$user->phys_address_l3 = $data['phys_address_l3'];
				$user->phys_address_l4 = $data['phys_address_l4'];
				$user->phys_area_code = $data['phys_area_code'];
				$user->post_address_l1 = $data['post_address_l1'];
				$user->post_address_l2 = $data['post_address_l2'];
				$user->post_address_l3 = $data['post_address_l3'];
				$user->post_address_l4 = $data['post_address_l4'];
				$user->post_code = $data['post_code'];
				$user->role_id = $data["role_id"];
				
				// Save User Data
				$user->save();
			}
			
			// Redirect
			return redirect("/admin/userlist");
		}
		
		// Invalid User View
		return view('errors.usernotfound', ['user_id' => $user_id]);
    }
	
	/**
     * Show User View View
     */
    public function getUserEditPassword($user_id) {
        return view('admin.usereditpass', ['name' => 'admin_useredit', 'user' => User::find($user_id)]);
    }
	
	/**
     * Handle a user edit request
     *
     * @param \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postUserEditPassword(Request $request) {
        // Get Posted Data
        $data = $request->all();
        
        // Get User ID
        $user_id = $data["user_id"];
        
        // Get User Object
        $user = User::find($user_id);
		
		// Only continue if we have a valid user
		if ($user) {
			if (isset($data['password']) && isset($data['password_confirmation'])) {
				if ($data['password'] === $data['password_confirmation']) {
					// Set encrypted password
					$user->password = bcrypt($data['password']);
					
					// Save User Data
					$user->save();
				}
			}
			
			// Redirect
			return redirect("/admin/useredit/{$user_id}");
		}
		
		// Invalid User View
		return view('errors.usernotfound', ['user_id' => $user_id]);
    }
    
    /**
     * Show User Documents View
     */
    public function getUserDocs($user_id) {
        // Get User Object
        $user = User::find($user_id);
        
		// Only continue if we have a valid user
		if ($user) return view('admin.userdocs', ['name' => 'admin_userdocs', 'user' => $user, 'documents' => $user->documents()->orderBy('updated_at', 'desc')->get()]);
		
		// Invalid User View
		return view('errors.usernotfound', ['user_id' => $user_id]);
    }
    
    /**
     * Handle viewing and downloading of Documents
     */
    public function getUserDocument($document_id,$document_filename) {
        // Get the Document
		$document = Document::find($document_id);
		
		// Only continue if a valid document record was found
		if ($document) {
			$user_id = $document->user_id;
			
			// Get User Object
			$user = User::find($user_id);
			
			// Only continue if we have a valid user
			if ($user) {
				// Get File Reference
				$file = Storage::disk('documents')->get('users/'.$user->id.'/'.$document->filename);
				
				// Output Response
				// Mime Type will tell the browser what to do with the file: Display or Download
				return (new Response($file, 200))->header('Content-Type', $document->mime_type);
			}
			// Invalid User View
			return view('errors.usernotfound', ['user_id' => $user_id]);
		}
		
		// Invalid Document View
		return view('errors.documentnotfound', ['document_id' => $document_id]);
	}
    
    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data,$pass) {
        return User::create([
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'title' => $data['title'],
            'role_id' => $data['role_id'],
            'tel_number' => $data['tel_number'],
            'fax_number' => $data['fax_number'],
            'phys_address_l1' => $data['phys_address_l1'],
            'phys_address_l2' => $data['phys_address_l2'],
            'phys_address_l3' => $data['phys_address_l3'],
            'phys_address_l4' => $data['phys_address_l4'],
            'phys_area_code' => $data['phys_area_code'],
            'post_address_l1' => $data['post_address_l1'],
            'post_address_l2' => $data['post_address_l2'],
            'post_address_l3' => $data['post_address_l3'],
            'post_address_l4' => $data['post_address_l4'],
            'post_code' => $data['post_code'],
            'email' => $data['email'],
            'password' => bcrypt($pass),
        ]);
    }
    
    /**
     * Send a password reset link to a user.
     *
     * @param  array  $credentials
     * @return string
     */
    protected function sendUserLoginDetails(array $credentials,$view) {
        Mail::send('emails.autopass', $credentials, function($message) use ($credentials)  {
            $message->to($credentials['email'], $credentials['first_name']." ".$credentials['last_name'])->subject('Welcome!');
        });
    }
}
