<?php
// Controller Namespace
namespace App\Http\Controllers\Auth;

// Components
use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

/**
 * AuthController defines all Authentication related functionality
 *
 * This controller handles the registration of new users, as well as the
 * authentication of existing users. By default, this controller uses
 * a simple trait to add these behaviors. Why don't you explore it?
 */
class AuthController extends Controller {
    // Traits
    use AuthenticatesAndRegistersUsers, ThrottlesLogins;
    
    // Redirect Overides
    protected $loginPath = '/auth/login';
    protected $redirectTo = '/';
    protected $redirectAfterLogout = '/';
    protected $redirectPath = '/users';
    
    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('guest', ['except' => ['getLogout']]);
    }
    
    /**
     * Display Login View
     */
    public function getLogin() {
        return view('auth.login', ['name' => 'login']);
    }
    
    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data) {
        return User::create([
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'title' => $data['title'],
            'role_id' => $data['role_id'],
            'tel_number' => $data['tel_number'],
            'fax_number' => $data['fax_number'],
            'phys_address_l1' => $data['phys_address_l1'],
            'phys_address_l2' => $data['phys_address_l2'],
            'phys_address_l3' => $data['phys_address_l3'],
            'phys_address_l4' => $data['phys_address_l4'],
            'phys_area_code' => $data['phys_area_code'],
            'post_address_l1' => $data['post_address_l1'],
            'post_address_l2' => $data['post_address_l2'],
            'post_address_l3' => $data['post_address_l3'],
            'post_address_l4' => $data['post_address_l4'],
            'post_code' => $data['post_code'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }
}
