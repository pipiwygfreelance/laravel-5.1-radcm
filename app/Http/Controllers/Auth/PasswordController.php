<?php
// Controller Namespace
namespace App\Http\Controllers\Auth;

// Components
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;

/**
 * Password Reset Controller
 *
 * This controller is responsible for handling password reset requests
 * and uses a simple trait to include this behavior. You're free to
 * explore this trait and override any methods you wish to tweak.
 */
class PasswordController extends Controller {
    // Traits
    use ResetsPasswords;
    
    // Redirect Overrides
    protected $loginPath = '/auth/login';
    protected $redirectTo = '/';
    protected $redirectAfterLogout = '/';
    protected $redirectPath = '/users/dashboard';
    
    /**
     * Create a new password controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('guest');
    }
}
