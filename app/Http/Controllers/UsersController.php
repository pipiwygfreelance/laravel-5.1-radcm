<?php
// Controller Namespace
namespace App\Http\Controllers;

// Components
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

// Models
use App\User;
use App\Role;
use App\Document;

// Engines
use DB;
use Storage;
use File;

/**
 * The Users Controller defines all user based functionality
 */
class UsersController extends Controller {
    /**
     * Show and define functionality for the Dashboard View
     *
     * @return Response
     */
    public function dashboard() {
        return view('users/dashboard', ['name' => 'user_dashboard','user'=> Auth::user()]);
    }
    
    /**
     * Show and define functionality for the Documents View
     *
     * @return Response
     */
    public function getDocuments() {
        return view('users/documents', ['name' => 'user_documents', 'documents' => Auth::user()->documents()->orderBy('updated_at', 'desc')->get()]);
    }
    
    /**
     * Show and define functionality for the Edit View
     *
     * @return Response
     */
    public function edit() {
        return view('users/edit', ['name' => 'user_edit', 'user' => Auth::user()]);
    }
    
    /**
     * Show and define functionality for the User Profile View
     *
     * @return Response
     */
    public function profile() {
        return view('users/profile', ['name' => 'user_profile','user' => Auth::user()]);
    }
    
    /**
     * Show and define functionality for the Add Document View
     *
     * @return Response
     */
    public function getDocumentAdd() {
        return view('users/documentadd', ['name' => 'user_document_add','user' => Auth::user()]);
    }
    
    /**
     * Show and define functionality for the Delete Document View
     *
     * @return Response
     */
    public function getDocumentDel($document_id) {
        return view('users/documentdelete', ['name' => 'user_document_del','document' => Document::find($document_id)]);
    }
    
    /**
     * Handle a document delete request
     *
     * @param \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postDocumentDel(Request $request) {
        // Get Data
        $data = $request->all();
        
        // Get Document ID
        $document_id = $data["document_id"];
        
        // Find Document
        $document = Document::find($document_id);
        
		// Only continue if a valid document record was found
		if ($document) {
            // Delete physical file from storage
            File::delete(storage_path('documents').'/users/'.$document->user_id.'/'.$document->filename);
            
            // Delete User
            $document->delete();
            
            // Redirect
            return redirect("/users");
        }
        
		// Invalid Document View
		return view('errors.documentnotfound', ['document_id' => $document_id]);
    }
    
    /**
     * Handle Document Add post functionality
     *
     * @return Response
     */
    public function postDocumentAdd(Request $request) {
        // Get Posted Data
        $data = $request->all();
        
        // Get current Authed User
        $user = Auth::user();
        
        // Create a new Document Instance
        $document = new Document();
        
        // Validate input
        $validator = $document->validator($data);
        
        // Handle Validation Errors
        if ($validator->fails())
            $this->throwValidationException($request, $validator);
        else {
            // Get Posted File object
            $file = $request->file('document');
            
            // Get File Filename
            $filename = $file->getClientOriginalName();
            
            // Get File Mimetype
            $mimeType = $file->getMimeType();
            
            // Get a document with the same filename, in order to handle updated documents
            $exitstingDocument = Document::where('filename', '=', $filename)->first();
            
            // If an existing document entry exists, set the document object to
            // the existing document entry instead of creating new
            if ($exitstingDocument)
                $document = $exitstingDocument;
            
            // Set Document Field Values
            $document->title = $data['title'];
            $document->description = $data['description'];
            $document->filename = $filename;
            $document->mime_type = $mimeType;
            $document->user_id = $user->id;
            
            // Move uploaded file to the correct user document folder in storage
            $file->move(storage_path('documents').'/users/'.$user->id.'/', $filename);
            
            // Save the Record
            $document->save();
        }
        // Redirect
        return redirect("/users/documents");
    }
    
    /**
     * Handle viewing and downloading of Documents
     */
    public function getDocument($document_id,$document_filename) {
        // Get the Document
        $document = Document::find($document_id);
        
        // Get User Object
        $user = Auth::user();
        
        // Get File Reference
		$file = Storage::disk('documents')->get('users/'.$user->id.'/'.$document->filename);
        
        // Output Response
        // Mime Type will tell the browser what to do with the file: Display or Download
		return (new Response($file, 200))->header('Content-Type', $document->mime_type);
	}
    
    /**
     * Handle Edit post functionality
     *
     * @return Response
     */
    public function postEdit(Request $request) {
        // Get Posted Data
        $data = $request->all();
        
        // Get Authed User Object
        $user = Auth::user();
        
        // Validate input
        $validator = $user->editValidator($data);
        
        // Handle Validation Errors
        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        } else {
            // Validation suceeded. Update User Data
            $user->title = $data['title'];
            $user->first_name = $data['first_name'];
            $user->last_name = $data['last_name'];
            $user->email = $data['email'];
            $user->tel_number = $data['tel_number'];
            $user->fax_number = $data['fax_number'];
            $user->phys_address_l1 = $data['phys_address_l1'];
            $user->phys_address_l2 = $data['phys_address_l2'];
            $user->phys_address_l3 = $data['phys_address_l3'];
            $user->phys_address_l4 = $data['phys_address_l4'];
            $user->phys_area_code = $data['phys_area_code'];
            $user->post_address_l1 = $data['post_address_l1'];
            $user->post_address_l2 = $data['post_address_l2'];
            $user->post_address_l3 = $data['post_address_l3'];
            $user->post_address_l4 = $data['post_address_l4'];
            $user->post_code = $data['post_code'];
            // Save User Data
            $user->save();
        }
        return redirect("/users/profile");
    }
}
