<?php
// Controller Namespace
namespace App\Http\Controllers;

// Components
use App\Http\Controllers\Controller;

// Models
use App\User;

/**
 * A basic simple Controller that renders a view based on the name
 * passed during the request.
 */
class PagesController extends Controller {
    /**
     * Show a generic page.
     *
     * @param  string  $name
     * @return Response
     */
    public function showPage($name = "intro") {
        return view($name, ['name' => $name]);
    }
}
