<?php
// Namespace
namespace App\Http\Middleware;

// Components
use Illuminate\Contracts\Auth\Guard;
use Closure;

// Models
use App\User;
use App\Role;

// Admin Middleware
class Admin {
    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    public function __construct(Guard $auth) {
        $this->auth = $auth;
    }
    
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        if ($this->auth->guest())
            return (($request->ajax()) ? response('Unauthorized.', 401) : redirect()->guest('auth/login'));
        else {
            // Get the Auth User
            $authuser = $this->auth->user();
            
            // Get the User Object
            $user = User::find($authuser->id);
            
            // Get the User Role ID
            $user_role_id = $user->role_id;
            
            // Get the Role Object
            $role = Role::find($user_role_id);
            
            // Check if user has sufficient access, and if not, display unauthorized error
            if ($role->role_system_name != "admin")
                return view('errors.unauthorized');
        }
        return $next($request);
    }
}
