<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/**
 * Administration Routes
 */
// Dashboard
Route::get('/admin', [
    'middleware' => 'adminauth',
    'uses' => 'AdminController@index'
]);
// Add User Get
Route::get('/admin/useradd', [
    'middleware' => 'adminauth',
    'uses' => 'AdminController@getUserAdd'
]);
// Add User Post
Route::post('/admin/useradd', [
    'middleware' => 'adminauth',
    'uses' => 'AdminController@postUserAdd'
]);
// List Users Get
Route::get('/admin/userlist', [
    'middleware' => 'adminauth',
    'uses' => 'AdminController@getUserList'
]);
// Delete User Get
Route::get('/admin/userdel/{id}', [
    'middleware' => 'adminauth',
    'uses' => 'AdminController@getUserDel'
]);
// Delete User Post
Route::post('/admin/userdel', [
    'middleware' => 'adminauth',
    'uses' => 'AdminController@postUserDel'
]);
// View User Get
Route::get('/admin/userview/{user_id}', [
    'middleware' => 'adminauth',
    'uses' => 'AdminController@getUserView'
]);
// Edit User Get
Route::get('/admin/useredit/{user_id}', [
    'middleware' => 'adminauth',
    'uses' => 'AdminController@getUserEdit'
]);
// Edit User Post
Route::post('/admin/useredit', [
    'middleware' => 'adminauth',
    'uses' => 'AdminController@postUserEdit'
]);

Route::get('/admin/userdocs/delete/{document_id}', [
    'middleware' => 'adminauth',
    'uses' => 'AdminController@deleteUserDoc'
]);
Route::post('/admin/userdocs/delete', [
    'middleware' => 'adminauth',
    'uses' => 'AdminController@postDeleteUserDoc'
]);

// User Documents Get
Route::get('/admin/userdocs/{user_id}', [
    'middleware' => 'adminauth',
    'uses' => 'AdminController@getUserDocs'
]);
Route::get('/admin/documents/{document_id}/{document_filename}', ['as' => 'admingetuserdocument', 'uses' => 'AdminController@getUserDocument']);
// Edit User Password Get
Route::get('/admin/useredit/password/{user_id}', [
    'middleware' => 'adminauth',
    'uses' => 'AdminController@getUserEditPassword'
]);
// Edit User Password Post
Route::post('/admin/useredit/password', [
    'middleware' => 'adminauth',
    'uses' => 'AdminController@postUserEditPassword'
]);

/**
 * User Routes
 */
// Dashboard
Route::get('/users', [
    'middleware' => 'auth',
    'uses' => 'UsersController@dashboard'
]);
Route::get('/users/dashboard', [
    'middleware' => 'auth',
    'uses' => 'UsersController@dashboard'
]);
// User Profile
Route::get('/users/profile', [
    'middleware' => 'auth',
    'uses' => 'UsersController@profile'
]);
// Edit User Profile
Route::get('/users/edit', [
    'middleware' => 'auth',
    'uses' => 'UsersController@edit'
]);
// Edit User Profile
Route::post('/users/edit', [
    'middleware' => 'auth',
    'uses' => 'UsersController@postEdit'
]);
// Documents Get
Route::get('/users/documents', [
    'middleware' => 'auth',
    'uses' => 'UsersController@getDocuments'
]);
// Document Add
Route::get('/users/documentadd', [
    'middleware' => 'auth',
    'uses' => 'UsersController@getDocumentAdd'
]);
// Document Add
Route::post('/users/documentadd', [
    'middleware' => 'auth',
    'uses' => 'UsersController@postDocumentAdd'
]);
Route::get('/users/documents/{document_id}/{document_filename}', ['as' => 'getdocument', 'uses' => 'UsersController@getDocument']);
// Delete Document Get
Route::get('/users/documentdelete/{id}', [
    'middleware' => 'auth',
    'uses' => 'UsersController@getDocumentDel'
]);
// Delete Document Post
Route::post('/users/documentdelete', [
    'middleware' => 'auth',
    'uses' => 'UsersController@postDocumentDel'
]);


/**
 * Pages Routes
 */
// Home / Front Page
Route::get('/', 'PagesController@showPage');
// Generic Pages Route
Route::get('/{name}', 'PagesController@showPage');

/**
 * Authentication Routes
 */
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');
Route::get('password/email', 'Auth\PasswordController@getEmail');
Route::post('password/email', 'Auth\PasswordController@postEmail');
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');
