<?php
// Model Namespace
namespace App;

// Framework Components
use Illuminate\Database\Eloquent\Model;

// Include User model
use App\User;
use Validator;

/**
 * Role Model defines table to use and Model Relationships
 */
class Document extends Model {
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'documents';
    
    /**
     * The users belonging to the role
     */
    public function users() {
        return $this->hasMany('App\User');
    }
    
    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function validator(array $data) {
        return Validator::make($data, [
            'title'         => 'required|min:3|max:255',
            'description'   => 'required',
            'document'          => 'required|mimes:txt,doc,docx,pdf,xls,xlsx,rtf',
        ]);
    }
}
