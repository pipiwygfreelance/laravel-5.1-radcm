<?php
// Model Namespace
namespace App;

// Framework Components
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

// Include Role model
use App\Role;
use Validator;

/**
 * User Model defines table to use, mass assignable attributes, attributes excluded and Model Relationships
 */
class User extends Model implements AuthenticatableContract, CanResetPasswordContract {
    // Traits
    use Authenticatable, CanResetPassword;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['role_id','title','first_name', 'last_name', 'email', 'password', 'tel_number', 'fax_number', 'phys_address_l1', 'phys_address_l2', 'phys_address_l3', 'phys_address_l4', 'phys_area_code', 'post_address_l1', 'post_address_l2', 'post_address_l3', 'post_address_l4', 'post_code'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];
    
    /**
     * Define relationship to Role model
     */
    public function role() {
        return $this->belongsTo('App\Role');
    }
    
    /**
     * Define relationship to Role model
     */
    public function documents() {
        return $this->hasMany('App\Document');
    }
    
    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function editValidator(array $data) {
        return Validator::make($data, [
            'first_name' => 'required|min:3|max:50',
            'last_name' => 'required|min:3|max:50',
            'email' => 'required|email|max:255'
        ]);
    }
    
    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function addValidator(array $data) {
        return Validator::make($data, [
            'first_name' => 'required|min:3|max:50',
            'last_name' => 'required|min:3|max:50',
            'email' => 'required|email|max:255|unique:users'
        ]);
    }
}
