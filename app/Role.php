<?php
// Model Namespace
namespace App;

// Framework Components
use Illuminate\Database\Eloquent\Model;

// Include User model
use App\User;

/**
 * Role Model defines table to use and Model Relationships
 */
class Role extends Model {
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'roles';
    
    /**
     * The users belonging to the role
     */
    public function users() {
        return $this->hasMany('App\User');
    }
}
