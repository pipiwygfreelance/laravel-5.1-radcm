<?php
// Components
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
/**
 * This class modifies an existing users table by adding various fields required for this application.
 */
class AlterUsersTableWithRequiredFieldsAndAddForeignKeyIndexToRolesTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        // Modify the existing users table. Add fields required in project spec
        // Also add an index field to reference user_role
        Schema::table('users', function($table){
            $table->dropColumn('name');
            
            $table->integer('role_id')->after('id')->unsigned();
            $table->foreign('role_id')->references('id')->on('roles');
            
            $table->string('first_name', 50)->after('role_id');
            $table->string('last_name', 50)->after('first_name');
            $table->string('title', 8)->after('last_name')->nullable();
            $table->string('tel_number', 10)->after('remember_token')->nullable();
            $table->string('fax_number', 10)->after('tel_number')->nullable();
            
            $table->string('phys_address_l1', 160)->after('fax_number')->nullable();
            $table->string('phys_address_l2', 160)->after('phys_address_l1')->nullable();
            $table->string('phys_address_l3', 160)->after('phys_address_l2')->nullable();
            $table->string('phys_address_l4', 160)->after('phys_address_l3')->nullable();
            $table->string('phys_area_code', 6)->after('phys_address_l4')->nullable();
            $table->string('post_address_l1', 160)->after('phys_area_code')->nullable();
            $table->string('post_address_l2', 160)->after('post_address_l1')->nullable();
            $table->string('post_address_l3', 160)->after('post_address_l2')->nullable();
            $table->string('post_address_l4', 160)->after('post_address_l3')->nullable();
            $table->string('post_code', 6)->after('post_address_l4')->nullable();
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        // Revert the above table alterations
        Schema::table('users', function($table) {
            //$table->string('name')->after('id');
            $table->dropForeign('users_role_id_foreign');
            $table->dropColumn('role_id');
            $table->dropColumn('first_name');
            $table->dropColumn('last_name');
            $table->dropColumn('title');
            $table->dropColumn('tel_number');
            $table->dropColumn('fax_number');
            $table->dropColumn('phys_address_l1');
            $table->dropColumn('phys_address_l2');
            $table->dropColumn('phys_address_l3');
            $table->dropColumn('phys_address_l4');
            $table->dropColumn('phys_area_code');
            $table->dropColumn('post_address_l1');
            $table->dropColumn('post_address_l2');
            $table->dropColumn('post_address_l3');
            $table->dropColumn('post_address_l4');
            $table->dropColumn('post_code');
        });
    }
}
