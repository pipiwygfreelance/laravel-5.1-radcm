<?php
// Components
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
/**
 * This class modifies an existing users table by increasing the telephone
 * and fax field length
 */
class AlterUsersTableIncreaseTelFaxLength extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        // Modify the existing users table. Increase field length
        Schema::table('users', function($table) {
            $table->string('tel_number', 13)->change();
            $table->string('fax_number', 13)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        // Revert the above table alterations
        Schema::table('users', function($table) {
            $table->string('tel_number', 10)->change();
            $table->string('fax_number', 10)->change();
        });
    }
}
