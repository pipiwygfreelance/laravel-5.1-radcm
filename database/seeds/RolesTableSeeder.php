<?php
// components
use Illuminate\Database\Seeder;
/**
 * Seeder to insert the initial roles Admin and Client
 */
class RolesTableSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        DB::table('roles')->insert([
            'id' => 1,
            'role_system_name' => 'admin',
            'role_name' => 'Administrator',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),
        ]);
        DB::table('roles')->insert([
            'id' => 2,
            'role_system_name' => 'client',
            'role_name' => 'Client',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),
        ]);
    }
}
