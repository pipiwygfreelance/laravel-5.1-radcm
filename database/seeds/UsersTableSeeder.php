<?php
// Components
use Illuminate\Database\Seeder;
/**
 * Seeder to insert initial Admin and Client User into the Users table
 */
class UsersTableSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        DB::table('users')->insert([
            'id' => 1,
            'role_id' => 1,
            'first_name' => 'System',
            'last_name' => 'Admin',
            'title' => '',
            'email' => 'root@localhost',
            'password' => bcrypt('SJfgHY86^$'),
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),
        ]);
    }
}
